# Taller Parcial Elect. Desarrollo Web Avanzado II 2017-2
##### Profesor: Pablo Bejarano
##### Fecha de entrega: 30 de Agosto de 2017
##### El taller preparcial equivale al 15% del corte
#
#
El taller será desarrollado y enregado de manera individual mediante la adición del docente al repositorio con permisos de Administración. 

El taller busca reforzar sus competencias teoricas y prácticas en desarrollo web, cubriendo temas desde programación orientaa a objetos hasta, patrones de diseño arquitectónicos y de desarrollo.

## La idea - ¿Qué vamos a hacer?

Vamos a desarrollar una aplicación móvil y web para enviar mensajería instantanea, imagenes y audios. La aplicación requiere entonces de un sistema constituido por:

- Servicios Web REST
- Cliente Móvil (aplicación híbrida Ionic 3)
- Cliente Web ( Angular 4 / Electron )

## Requisitos del sistema

### Servicios Web (2 Puntos)
- Todos los servicios deben responder a la definición de servicios web RESTful
- Los servicios serán desarrollados a partir del archivo /Services/services.def.json escrito bajo la norma swagger 2.0
- Todas las respuestas serán dadas en formato JSON
- Todas las solicitudes responderán con el código de respuesta adecuado definido en la cabecera.
#### Bonificaciones
- Si la distribución de los servicios usa el patrón de micro servicios.

### Cliente Móvil (2 Puntos)
- Todas las entidades de la aplicación deben responder a la definición de modelos de datos (clases por entidad).
- Toda la información persistente y local de las entidades, debe ser accedida mediante servicios o proveedores, según considere usted más correcto.
- Los usuarios deben poder conservar una sesión local dentro de la aplicación.
- A diferencia de WhatsApp, esta aplicación pedirá un login al estilo Telegram para acceder a la misma.
- Los contactos de los usuarios agregados en el App serán almacenados en el servidor del App.
- Todas las imagenes serán almacenadas en el servidor después de ser procesadas para hacer una optimización en pesos con la menor perdida de calidad posible.
- Todos los audios serán almacenados en el servidor después de ser procesados para hacer una optimización en pesos con la menor perdida de calidad posible.
- Los usuarios no siempre tienen red, por ende la aplicación debe chekear el estado de la red y enviar los mensajes, imagenes, audios, contactos, localización, que estén pendientes en el momento de tener conexión.
- Cada usuario tiene: Foto, Nombre de usuario, Nombre, Apellido, correo, teléfono, estado (texto para tirar indirectas y hacer sarcasmos).
- Los usuarios pueden bloquear a otros usuarios o agregarlos cuando les llegan mensajes de un contacto no existente en su agenda.
- Los usuarios pueden bloquear a otros usuarios de la manera en que se bloquean en whatsapp

**Nota:** Todas las funcionalidades tendrán el look & feel de WhatsApp, funcionalidad que no lo tenga, NO VALE.
**Nota 2:** Existen plugins para acceder a galería, contactos, etc.

#### Bonificaciones
- Si la aplicación recibe notificaciones de los mensajes por push notifications (incluso cuando el app está en segundo plano).
- Si la aplicación muestra los estados del usuario (En Línea, Hablando con XXXX, Última conexión en: xx xx xxx)
- La aplicación permite buscar términos dentro de los chats.

### Cliente Web (1 Punto)
- Todas las funcionalidades del cliente móvil.
- El cliente web debe leer un código QR generado por la aplicación para hacer uso del mismo (NO TIENE LOGIN).

## Investigación (Parte Teórica)
1. Patrónes de diseño arquitectónicos (Seleccione y use el mejor patrón o la mejor combinación de patrones arquitectónicos en su sistema, justifique su elección en el archivo ArchitecturalDecisions.md).
    + Publish Subscribe
    + MVC
    + Broker
    + Micro Services

    **La justificación de los patrones arquitectónicos implementados podrá disminuir su calificación hasta en 2.5, un diseño arquitectónico sobresaliente será bonificado con 0.5**

2. Patrónes de diseño (Seleccione e implemente los patrones de diseño que le parezcan mejores en su solución, justifique su elección en el archivo DesignPatterns.md).
    + Creacionales
        + Factory Method
        + Abstact Factory
        + Singleton
        + Builder
        + Prototype
    + Estructurales
        + Adapter
        + Bridge
        + Facade
        + Decorator
        + Proxy
    + Comportamiento
        + Memento
        + Iterator
        + Observer
        + Visitor

    **La justificación de los patrones implementados podrá disminuir su calificación hasta en 2.5, un planteamiento sobresaliente será bonificado con 0.5**

3. Programación orientada a objetos (diligencie su investigación en OOP.md y de ejemplos)
    + Encapsulación
    + Herencia
    + Polimorfismo
    + Interfacez
    + Clases Estáticas
    + Métodos Estáticos
    + Clases Abstractas
    + Métodos Abstractos
    + Constructores
    + Sobrecarga de un método
    + Presistencia

    **Cada error encontrado en el desarrollo que incumpla los lineamientos de la programación orientada a objetos de manera injustificable o arbitraria será penalizado con 0.5 de la nota**

Si sus patrónes arquitectónicos seleccionados son muy adecuados pero la implementación le es muy complicada, no se preocupe mientras su justificacicón sea implecable e incluya los factores por los cuales no podrá realizar la implementación completa (no vale "porque no se").

### Bonificaciones
- Si los archivos md estan escritos hermosos + 0,2 por archivo

####Nota: OJO!
El módelo de datos y el archivo de servicios (swager 2.0) será definido por cada uno de ustedes para cumplir con las necesidades del proyecto. Ayudense con MySQL Workbench y https://studio.restlet.com/