import { Co.Edu.Whatsapp.WebPage } from './app.po';

describe('co.edu.whatsapp.web App', () => {
  let page: Co.Edu.Whatsapp.WebPage;

  beforeEach(() => {
    page = new Co.Edu.Whatsapp.WebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
