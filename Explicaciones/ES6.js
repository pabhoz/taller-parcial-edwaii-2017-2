/* Usando Map */
let usuarios = [
    {username:"pabhoz",password:"1234",edad:25},
    {username:"paobox",password:"1234",edad:21},
    {username:"luloise",password:"1234",edad:20},
    {username:"satiz",password:"1234",edad:24}
];

//Queremos saber la diferencia de edad de todos los usuarios con respecto a una edad definida
// en este caso la mayoría de edad colombiana (18)

let diferencias = usuarios.map(function(user){
    return user.edad - 18;
});

console.log(diferencias);

/* Funciones flecha */
//Las funciones flecha son un atajo a las funciones normales y tienen la ventaja de que heredan el scope del Padre.

/*let btn = document.createElement("button"); 

function customScope(){ console.log(this); }

btn.onclick = function(){
	console.log(this);
    customScope();
};

let btn2 = document.createElement("button");
btn2.onclick = () => { console.log(this) };

btn.click();
btn2.click();*/

// Ejecutar esto en la consola de chrome porque acá no hay HTML implicado