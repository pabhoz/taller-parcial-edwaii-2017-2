<?php

class Index_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }
    
   
    public function getIndex() {
        Request::setHeader(202, "text/html");
       
        echo "Get method Index controller";
    }

    public function postIndex() {
        $data = $_POST;
        
        
        $result = Database::insert("route",$data);
        echo json_encode($result);
    }

    public function getSaludo($nombre, $apellido) {
        if (!isset($nombre) || !isset($apellido)) {
            throw new Exception('Paremetros insuficientes.');
        }
        Request::setHeader(200, "text/plain");
        echo "Hey " . $nombre . " " . $apellido . "!";
    }
    
    public function getDumpData(){
        $users = [
            ["id"=>1,"name"=>"Pablo", "age"=>"25","gender"=>"Male"],
            ["id"=>2,"name"=>"Paola", "age"=>"21", "gender"=>"Female"],
            ["id"=>3,"name"=>"Lu", "age"=>"20", "gender"=>"Female"],
            ["id"=>4,"name"=>"Sati", "age"=>"24", "gender"=>"Male"]
        ];
        print json_encode($users);
    }
    
    public function getById($id){
        $users = [
            ["id"=>1,"name"=>"Pablo", "age"=>"25","gender"=>"Male"],
            ["id"=>2,"name"=>"Paola", "age"=>"21", "gender"=>"Female"],
            ["id"=>3,"name"=>"Lu", "age"=>"20", "gender"=>"Female"],
            ["id"=>4,"name"=>"Sati", "age"=>"24", "gender"=>"Male"]
        ];
        print json_encode($users[$id-1]);
    }
}
